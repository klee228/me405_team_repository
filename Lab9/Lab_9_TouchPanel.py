'''
@file Lab_9_TouchPanel.py
@brief A file reads position using a resistive touchpad
@details A file that contains a class that scans the x,y, and z location of 
the object on the resistive touch panel
@author Kevin Lee
@date March 2, 2021
'''

import pyb
import utime

class TouchPanel():
    '''
    @brief class that implements the touch panel
    @details class that determines the location of the object on the touch panel
    '''
        
    def __init__(self, xp,xm,yp,ym,length,width,center_coords):
        '''
        @brief constructor for a TouchPanel object
        @details initializes all the variables and takes pins
        @param xp pyb.Pin for xp
        @param xm pyb.Pin for xm
        @param yp pyb.Pin for yp
        @param ym pyb.Pin for ym
        @param length x dimension
        @param width y dimension
        @param center_coords list of coordinates of center [x,y]
        '''
        ##positive x pin
        self.xp = pyb.Pin(xp, mode=pyb.Pin.OUT_PP, value= 0)
        ##negative x pin
        self.xm = pyb.Pin(xm, mode=pyb.Pin.OUT_PP, value= 0)
        ## positive y pin
        self.yp = pyb.Pin(yp, mode=pyb.Pin.OUT_PP, value= 0)
        ##negative y pin
        self.ym = pyb.Pin(ym, mode=pyb.Pin.OUT_PP, value= 0)
        ##panel length
        self.l = length
        ##panel width
        self.w = width
        ##list panel center [x,y]
        self.cc = center_coords
    
    def scan_x(self):
        '''
        @brief scans the x location of the ball
        @details has a 10 us delay to account for settling time
        @return float x position of the ball
        '''
        self.xm.init(mode=pyb.Pin.OUT_PP, value=0)
        self.xp.init(mode=pyb.Pin.OUT_PP, value=1)
        self.ym.init(mode=pyb.Pin.IN)
        self.yp.init(mode=pyb.Pin.IN)
        
        self.ym.init(mode=pyb.Pin.ANALOG)
        self.delay(10)
        ##x adc object
        adc_x = pyb.ADC(self.ym)
        return((adc_x.read()-4095/2)*self.l/4095+self.cc[0])
    
    def scan_y(self):
        '''
        @brief scans the y location of the ball
        @details has a 10 us delay to account for settling time
        @return float y position of the ball
        '''
        self.ym.init(mode=pyb.Pin.OUT_PP, value=0)
        self.yp.init(mode=pyb.Pin.OUT_PP, value=1)
        self.xm.init(mode=pyb.Pin.IN)
        self.xp.init(mode=pyb.Pin.IN)
        
        self.xm.init(mode=pyb.Pin.ANALOG)
        self.delay(10)
        ##y adc object
        adc_y = pyb.ADC(self.xm)
        return((adc_y.read()-4095/2)*self.w/4095+self.cc[1])
    
    def scan_z(self):
        '''
        @brief scans if something is on the touch panel
        @details has a 10 us delay to account for settling time
        @return boolean if something is on the panel
        '''
        self.xm.init(mode=pyb.Pin.OUT_PP, value=0)
        self.yp.init(mode=pyb.Pin.OUT_PP, value=1)
        self.ym.init(mode=pyb.Pin.IN)
        self.xp.init(mode=pyb.Pin.IN)
        
        self.ym.init(mode=pyb.Pin.ANALOG)
        self.delay(10)
        ##z adc obect
        adc_z = pyb.ADC(self.ym)
        z_reading = adc_z.read()
        if z_reading >4000:
            return(False)
        return(True)
    
    def scan(self):
        '''
        @brief scans the location of the ball
        @details must take less than 1500us to work with other code
        @return tuple (x,y,z) where x and y are positions and z is whether something is on the platform
        '''
        return((self.scan_x(), self.scan_y(), self.scan_z()))
    
    def delay(self, t):
        '''
        @brief delays the code for t us
        @details used to account for the settling time of the system. NOTE: at a low delay, makes no difference in execution time.
        '''
        ## time that the program should resume sccanning
        end_time = utime.ticks_add(utime.ticks_us(),t)
        while utime.ticks_diff(utime.ticks_us(), end_time) <0:
            pass

# t = TouchPanel(pyb.Pin.board.PA7, pyb.Pin.board.PA1, pyb.Pin.board.PA6, pyb.Pin.board.PA0,7.5, 4.5, [0,0])
# while True:
#     print(t.scan())

# start = utime.ticks_us()
# for i in range(1000):
#     print(str(i) + ' ' + str(t.scan()))
# print('Average response time (us): ' + str(utime.ticks_diff(utime.ticks_us(), start)/1000))